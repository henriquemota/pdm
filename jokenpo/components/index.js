import { MyTitle } from './my-title'
import { MyBoard } from './my-boad'
import { MyBoardPanel } from './my-board-panel'
import { MyButton } from './my-button'

export { MyTitle, MyBoard, MyBoardPanel, MyButton }
