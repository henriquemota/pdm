import React from 'react'
import { Card, Button, TextInput, Paragraph } from 'react-native-paper'
import axios from 'axios'
import { ContainerScroll } from '../components'

export class Cep extends React.Component {
	state = {
		cep: null,
		endereco: null,
		carregando: false,
	}
	render() {
		return (
			<ContainerScroll>
				<Card>
					<Card.Title title="Busca CEP" />
					<Card.Content>
						<TextInput label="CEP" keyboardType="number-pad" onChangeText={cep => this.setState({ cep })} />
					</Card.Content>
					<Card.Actions>
						<Button loading={this.state.carregando} mode="outlined" onPress={() => this.getAddress()}>
							pesquisar
						</Button>
					</Card.Actions>
				</Card>
				{this.state.endereco && <DetalhesEndereco endereco={this.state.endereco} />}
			</ContainerScroll>
		)
	}

	getAddress() {
		this.setState({ carregando: true })
		axios
			.get(`https://viacep.com.br/ws/${this.state.cep}/json/`)
			.then(res => this.setState({ endereco: res.data, carregando: false }))
			.catch(err => this.setState({ carregando: false }))
	}
}

function DetalhesEndereco(props) {
	const { endereco } = props || null
	return (
		<Card style={{ marginTop: 20 }}>
			<Card.Title title="Dados do CEP" />
			<Card.Content>
				<Paragraph>Localidade: {endereco.localidade}</Paragraph>
				<Paragraph>UF: {endereco.uf}</Paragraph>
				<Paragraph>Logradouro: {endereco.logradouro}</Paragraph>
				<Paragraph>Complemento: {endereco.complemento}</Paragraph>
				<Paragraph>Bairro: {endereco.bairro}</Paragraph>
				<Paragraph>CEP: {endereco.cep}</Paragraph>
			</Card.Content>
		</Card>
	)
}
