import React from 'react'
import { Text, View } from 'react-native'
import { SQLite } from 'expo-sqlite'

export class Database extends React.Component {
	state = {
		items: [],
	}

	db = SQLite.openDatabase('database.db')

	componentWillMount() {
		this.db.transaction(tx => {
			tx.executeSql('create table if not exists items (id integer primary key not null, done int, value text);')
		})
		this.get()
	}

	get() {
		this.db.transaction(tx =>
			tx.executeSql('select id from  items where id = ?', [3], (a, b) => {
				this.setState({ items: b.rows })
			}),
		)
	}

	// add(text) {
	// 	if (text === null || text === '') return false
	// 	this.db.transaction(tx => {
	// 		tx.executeSql('insert into items (done, value) values (0, ?)', [text])
	// 		// tx.executeSql('select * from items', [], (a, b) => console.log(b))
	// 	})
	// }

	render() {
		const { items } = this.state
		console.log(items)
		return (
			<View>
				<Text>Titulo</Text>
			</View>
		)
	}
}
