import { Home } from './home'
import { Cep } from './cep'
import { Tasks } from './tasks'
import { Database } from './database'

export { Home, Cep, Tasks, Database }
