import React from 'react'
import { ActivityIndicator, Modal } from 'react-native'
import { Button, Card, TextInput } from 'react-native-paper'
import { ContainerScroll } from '../components'
import { TaskService } from '../services'

export class Tasks extends React.Component {
	state = {
		loading: false,
		data: [],
		text: '',
		modal: false,
	}

	componentWillMount() {
		this.setState({ loading: true })
		TaskService.getTasks(e => this.setState({ data: e, loading: false }))
	}

	render() {
		return (
			<ContainerScroll>
				<Button mode="contained" onPress={() => this.setState({ modal: true, text: '' })}>
					Nova tarefa
				</Button>
				<Modal animationType="slide" transparent={false} visible={this.state.modal}>
					<Card style={{ marginTop: 25 }}>
						<Card.Title title="Cadastrar tarefa" />
						<Card.Content>
							<TextInput label="Tarefa" value={this.state.text} onChangeText={text => this.setState({ text })} />
						</Card.Content>
						<Card.Actions>
							<Button
								mode="outlined"
								style={{ marginRight: 5 }}
								onPress={() => {
									this.setState({ loading: true, modal: false })
									TaskService.saveTasks({ description: this.state.text })
									TaskService.getTasks(e => this.setState({ data: e, loading: false }))
								}}>
								Gravar
							</Button>
							<Button mode="outlined" onPress={() => this.setState({ modal: false })}>
								Cancelar
							</Button>
						</Card.Actions>
					</Card>
				</Modal>
				{this.state.loading && <ActivityIndicator animating={this.state.loading} size="large" color="#0000ff" />}
				{this.state.data &&
					this.state.data.map((e, i) => (
						<Card style={{ marginTop: 5 }} key={i}>
							<Card.Title title={e.description} />
							<Card.Actions>
								<Button
									onPress={() => {
										this.setState({ loading: true })
										TaskService.deleteTasks(e.key)
										TaskService.getTasks(e => this.setState({ data: e, loading: false }))
									}}>
									Excluir
								</Button>
							</Card.Actions>
						</Card>
					))}
			</ContainerScroll>
		)
	}
}
