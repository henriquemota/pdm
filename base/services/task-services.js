import { API_URL } from '../config'
import axios from 'axios'

export class TaskService {
	static getTasks(callback) {
		axios
			.get(`${API_URL}/tasks.json`)
			.then(e => {
				let res = []
				Object.entries(e.data).map(e => {
					let item = e[1]
					item['key'] = e[0]
					res.push(item)
				})
				callback(res)
			})
			.catch(error => {
				callback([])
			})
	}

	static saveTasks(data) {
		axios.post(`${API_URL}/tasks.json`, data)
	}

	static deleteTasks(key) {
		axios.delete(`${API_URL}/tasks/${key}.json`)
	}
}
