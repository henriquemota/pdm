import React from 'react'
import { SafeAreaView, ScrollView } from 'react-native'

export function ContainerScroll(props) {
	return (
		<SafeAreaView>
			<ScrollView style={[styles.container, { flexDirection: props.row ? 'row' : 'column' }]}>
				{props.children}
			</ScrollView>
		</SafeAreaView>
	)
}

const styles = {
	container: {
		flexWrap: 'wrap',
		padding: 10,
	},
}
