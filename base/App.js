import React from 'react'
import { Router, Tabs, Stack, Scene } from 'react-native-router-flux'
import { Ionicons, FontAwesome } from '@expo/vector-icons'
import { Home, Cep, Tasks, Database } from './views'

export default class App extends React.Component {
	render() {
		return (
			<Router>
				<Tabs>
					<Scene key="home" component={Home} title="Home" icon={() => <Ionicons name="ios-home" size={24} />} />
					<Scene key="cep" component={Cep} title="CEP" icon={() => <Ionicons name="ios-search" size={24} />} />
					<Scene key="tasks" component={Tasks} title="Tarefas" icon={() => <FontAwesome name="tasks" size={24} />} />
					<Scene
						key="database"
						component={Database}
						title="Database"
						icon={() => <FontAwesome name="database" size={24} />}
					/>
				</Tabs>
			</Router>
		)
	}
}
